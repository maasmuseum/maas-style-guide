# MAAS Style Guide
Shared colours, paddings, logos and other stuff for use in MAAS projects. Currently in used by collection.maas.museum and new.maas.museum.

## Getting Started
Add `"maas-style-guide": "git+https://git@bitbucket.org/maasmuseum/maas-style-guide.git#v1.X.X"` to your `package.json` file.

## Versioning
If you do any updates, do the usual git stuff and make sure you version this package.
```
$ git add .
$ git commit -am "Update"
$ git push
$ npm version patch|minor|major
$ git push --tags
```

## SCSS
In /scss folder.

### base.scss
For styling HTML tags only. Be very careful! This also brings in normalize.css.

### variables.scss
Global SASS variables. eg. colours, font sizes, spacings etc.

### helpers.scss
General utility classes. eg. .container, .button etc.

### mixins.scss
SASS functions

## Logos
In /logos folder.
